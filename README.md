# Vue Forum

## Project info
```
Single Page Application built with Vue.js, Vuetify and Firebase
Vuex is used for state management
```

### Features
```
- topic listing with lazy load
- creating a topic
- authorization and authentication
- autologin
- register a new user
- profile overview
- profile edit
- reset password
- my topics
- filter by category
- tags
```
