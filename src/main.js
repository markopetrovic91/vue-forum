import "babel-polyfill";
import Vue from "vue";
import * as firebase from "firebase";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    firebase.initializeApp({
      apiKey: "AIzaSyCUIMaI-uBw4h66H3k8hnfxS2nCOgX0wUc",
      authDomain: "spa-forum-fea77.firebaseapp.com",
      databaseURL: "https://spa-forum-fea77.firebaseio.com",
      projectId: "spa-forum-fea77",
      storageBucket: "spa-forum-fea77.appspot.com"
    });
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.$store.dispatch("autoSignIn", user);
      }
    });
    this.$store.dispatch("initialLoadTopics");
  }
}).$mount("#app");
