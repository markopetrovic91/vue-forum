import Vue from "vue";
import Vuex from "vuex";
import * as firebase from "firebase";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loadedTopics: [],
    user: null,
    notification: {
      type: "",
      message: "",
      display: false
    },
    error: null,
    loading: false,
    categories: [
      "World",
      "Business",
      "Science",
      "Sport",
      "Books",
      "Technology",
      "Lifestyle"
    ],
    tags: [
      "business",
      "sport",
      "world",
      "science",
      "books",
      "lifestyle",
      "technology"
    ],
    wrotedTopics: []
  },
  mutations: {
    SET_USER(state, payload) {
      state.user = payload;
    },
    SET_ERROR(state, payload) {
      state.error = payload;
    },
    CLEAR_ERROR(state) {
      state.error = null;
    },
    SET_LOADING(state, payload) {
      state.loading = payload;
    },
    SET_TOPIC(state, payload) {
      state.loadedTopics.push(payload);
    },
    SET_LOADED_TOPICS(state, payload) {
      state.loadedTopics = payload;
    },
    SET_WROTED_TOPICS(state, payload) {
      state.wrotedTopics = payload;
    },
    UPDATE_USER_PROFILE(state, payload) {
      state.user.username = payload.username;
      state.user.firstname = payload.firstname;
      state.user.lastname = payload.lastname;
    },
    NOTIFY(state, payload) {
      state.notification.display = true;
      state.notification.type = payload.type;
      state.notification.message = payload.message;

      setTimeout(() => {
        state.notification.display = false;
      }, 5000);
    },
    SET_COMMENT(state, payload) {
      state.topics[payload.topicId].comments.push({});
    }
  },
  actions: {
    signUserUp({ commit }, payload) {
      commit("SET_LOADING", true);
      commit("CLEAR_ERROR");
      firebase
        .auth()
        .createUserWithEmailAndPassword(payload.email, payload.password)
        .then(user => {
          commit("SET_LOADING", false);
          const newUser = {
            id: user.user.uid,
            email: payload.email
          };
          commit("SET_USER", newUser);
          firebase
            .database()
            .ref("users")
            .child(newUser.id)
            .set({
              username: "",
              firstname: "",
              lastname: ""
            })
            .then(data => {
              const notify = {
                type: "success",
                message: `You have been successfully logged in`
              };
              commit("NOTIFY", notify);
            })
            .catch(error => {
              const notify = {
                type: "error",
                message: `${error.message}`
              };
              commit("NOTIFY", notify);
            });
        })
        .catch(error => {
          commit("SET_ERROR", error);
          commit("SET_LOADING", false);
          const notify = {
            display: true,
            type: "error",
            message: `${error.message}`
          };
          commit("NOTIFY", notify);
        });
    },
    signUserIn({ commit }, payload) {
      commit("SET_LOADING", true);
      firebase
        .auth()
        .signInWithEmailAndPassword(payload.email, payload.password)
        .then(user => {
          commit("SET_LOADING", false);
          const loggedUser = {
            id: user.user.uid,
            email: user.user.email,
            meta: {
              lastlogin: user.user.metadata.lastSignInTime,
              creationTime: user.user.metadata.creationTime
            }
          };
          commit("SET_USER", loggedUser);
          const notify = {
            type: "success",
            message: `You have been successfully logged in`
          };
          commit("NOTIFY", notify);
        })
        .catch(error => {
          const notify = {
            type: "error",
            message: `${error.message}`
          };
          commit("SET_LOADING", false);
          commit("NOTIFY", notify);
        });
    },
    updateUser({ commit }, payload) {
      // here goes email update
      // const user = firebase.auth().currentUser;
      // user.updateEmail("test3@test.com").then(res => {
      //     console.log(res);
      //     commit("UPDATE_USER_EMAIL", res.email);
      //     console.log("updated email");
      commit("SET_LOADING", true);
      firebase
        .database()
        .ref("users")
        .child(payload.id)
        .set({
          username: payload.username,
          firstname: payload.firstname,
          lastname: payload.lastname
        })
        .then(data => {
          commit("UPDATE_USER_PROFILE", payload);
          const notify = {
            type: "success",
            message: "Profile has been successfully updated!"
          };
          commit("NOTIFY", notify);
        })
        .catch(error => {
          const notify = {
            type: "error",
            message: `Error: ${error.message}`
          };
          commit("NOTIFY", notify);
        });
      commit("SET_LOADING", false);
    },
    createNewTopic({ commit }, payload) {
      commit("SET_LOADING", true);
      commit("CLEAR_ERROR");
      firebase
        .database()
        .ref("topics")
        .push(payload)
        .then(data => {
          const key = data.key;
          commit("SET_TOPIC", {
            ...payload,
            id: key
          });
          const notify = {
            type: "success",
            message: `New topic has been created`
          };
          commit("NOTIFY", notify);
        })
        .catch(error => {
          const notify = {
            type: "error",
            message: `Error: ${error.message}`
          };
          commit("NOTIFY", notify);
        });

      commit("SET_LOADING", false);
    },
    initialLoadTopics({ commit }) {
      commit("SET_LOADING", true);
      firebase
        .database()
        .ref("topics")
        // .on(
        //   "value",
        //   function(snapshot) {
        //     const topics = [];
        //     const obj = snapshot.val();
        //     for (let key in obj) {
        //       topics.push({
        //         id: key,
        //         title: obj[key].title,
        //         text: obj[key].text,
        //         tags: obj[key].tags,
        //         comments: obj[key].comments,
        //         published: obj[key].published,
        //         date: obj[key].date,
        //         category: obj[key].category,
        //         userId: obj[key].userId
        //       });
        //     }
        //     commit("SET_LOADED_TOPICS", topics);
        //     commit("SET_LOADING", false);
        //   },
        //   function(error) {
        //     console.log("The read failed: " + error.message);
        //     commit("SET_LOADING", false);
        //   }
        // );
        .once("value")
        .then(res => {
          const topics = [];
          const obj = res.val();
          for (let key in obj) {
            topics.push({
              id: key,
              title: obj[key].title,
              text: obj[key].text,
              tags: obj[key].tags,
              comments: obj[key].comments,
              published: obj[key].published,
              date: obj[key].date,
              category: obj[key].category,
              userId: obj[key].userId
            });
          }
          commit("SET_LOADED_TOPICS", topics);
          commit("SET_LOADING", false);
        })
        .catch(error => {
          console.log(error);
        });
    },
    autoSignIn({ commit }, payload) {
      commit("SET_LOADING", true);
      firebase
        .database()
        .ref("users")
        .child(payload.uid)
        .once("value")
        .then(res => {
          const rest = res.val();
          const usr = {
            id: payload.uid,
            email: payload.email,
            ...rest
          };
          commit("SET_USER", usr);
          commit("SET_LOADING", false);
          const notify = {
            type: "success",
            message: `You have been successfully logged in`
          };
          commit("NOTIFY", notify);
        })
        .catch(error => {
          const notify = {
            type: "error",
            message: `${error.message}`
          };
          commit("NOTIFY", notify);
        });
    },
    logout({ commit }) {
      firebase.auth().signOut();
      commit("SET_USER", null);
      const notify = {
        type: "success",
        message: `You're signed out`
      };
      commit("NOTIFY", notify);
    },
    setWrotedTopics({ commit }, payload) {
      commit("SET_LOADING", true);
      firebase
        .database()
        .ref("topics")
        .orderByChild("userId")
        .startAt(payload)
        .endAt(payload)
        .once("value", function(snapshot) {
          const topics = [];
          const obj = snapshot.val();
          for (let key in obj) {
            topics.push({
              id: key,
              title: obj[key].title,
              text: obj[key].text,
              tags: obj[key].tags,
              published: obj[key].published,
              date: obj[key].date,
              category: obj[key].category,
              userId: obj[key].userId
            });
          }
          commit("SET_WROTED_TOPICS", topics);
          commit("SET_LOADING", false);
        });
    },
    createNewComment({ commit }, payload) {
      commit("SET_LOADING", true);
      firebase
        .database()
        .ref("topics")
        .child(payload.topicId + "/comments")
        .push({
          text: payload.text,
          userId: payload.userId,
          username: payload.username
        })
        .then(res => {
          const notify = {
            type: "success",
            message: "You have successfully commented. Thank you!"
          };
          commit("NOTIFY", notify);
          commit("SET_COMMENT", payload);
          return true;
        })
        .catch(error => {
          const notify = {
            type: "error",
            message: `nesto nije kako treba: ${error.message}`
          };
          commit("NOTIFY", notify);
          return false;
        });
      commit("SET_LOADING", false);
    }
  },
  getters: {
    loadedTopics(state) {
      return state.loadedTopics.sort((topicA, topicB) => {
        return topicA.data > topicB.date;
      });
    },
    loadedTopic(state) {
      return topicId => {
        return state.loadedTopics.find(topic => topic.id === topicId);
      };
    },
    featuredTopics(state) {
      return state.loadedTopics.reverse().slice(0, 6);
    },
    user(state) {
      return state.user;
    },
    error(state) {
      return state.error;
    },
    loading(state) {
      return state.loading;
    },
    categories(state) {
      return state.categories;
    },
    tags(state) {
      return state.tags;
    },
    getWrotedTopics(state) {
      return state.wrotedTopics;
    },
    getNotification(state) {
      return state.notification;
    }
  }
});
