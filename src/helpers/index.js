export function excerpt(text, numWords = 10) {
  return text
    .toString()
    .split(" ")
    .slice(0, numWords)
    .join(" ")
    .replace(/(^,)|(,$)/g, "")
    .concat("...");
}
