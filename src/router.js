import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home.vue";
import SignIn from "@/views/SignIn";
import SignUp from "@/views/SignUp";
import Topics from "@/views/Topics";
import Topic from "@/views/Topic";
import NewTopic from "@/views/NewTopic";
import Profile from "@/views/Profile";
import AuthGuard from "./auth-guard";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/categories",
      name: "categories",
      // route level code-splitting
      // this generates a separate chunk (category.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "category" */ "./views/Category.vue")
    },
    {
      path: "/signin",
      component: SignIn,
      name: "signin"
    },
    {
      path: "/register",
      component: SignUp,
      name: "register"
    },
    {
      path: "/section/:sectionId",
      component: Topic,
      name: "section"
    },
    {
      path: "/topics/:topicId",
      component: Topic,
      name: "topic",
      params: true
    },
    {
      path: "/topic/new",
      component: NewTopic,
      name: "newtopic",
      beforeEnter: AuthGuard
    },
    {
      path: "/all-topics",
      name: "alltopics",
      component: Topics
    },
    {
      path: "/profile",
      component: Profile,
      name: "profile",
      beforeEnter: AuthGuard
    }
  ]
});
