import Vue from "vue";
import Vuetify from "vuetify/lib";
import "vuetify/src/stylus/app.styl";

Vue.use(Vuetify, {
  iconfont: "md",
  theme: {
    primary: "#71808D",
    secondary: "#FBE58C",
    accent: "#22313F",
    error: "#FF3C38",
    info: "#DBE2F1",
    success: "#66ABB0",
    warning: "#FBE58C"
  }
});
